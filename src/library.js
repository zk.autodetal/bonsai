//......................................................................................

export function inject_object(obj_a, obj_b) {
    for (let kb of Object.keys(obj_b)) {
        if (Object.keys(obj_a).includes(kb)) {
            obj_a[kb] = obj_b[kb]
        }
    }
}

//......................................................................................

export class TreeNode {



    constructor(name, opts = {}) {   
        this.name     = name
        this.marked   = false
        this.enabled  = true
        this.selected = false
        this.unfolded = false
        this.children = []
        this.content  = []
        this.mode     = "folder"
        inject_object(this, opts)
    }

    async loadIndentedFile(filename) {
        let responce  = await fetch(filename)
        let text_data = (await responce.text()).split("\n")
        this.appendIndentedStructure(text_data)
    }

    appendIndentedStructure(text_data) {
        let node_pool = []
        node_pool.push(this)
        for (let line of text_data) {
            if (line.trim() == "") continue
            let tcnt = count_tabs(line)
            let node = new TreeNode(line.trim())
            node_pool[tcnt].children.push(node)
            node_pool[tcnt + 1] = node
        }
    }

    appendNodes(...args) {
        this.children.push(...args)
    }

    appendTree(...tree) {
        for (let tx of tree) {
            let node = new TreeNode(tx.name)
            this.appendNodes(node)
            if (tx.children && tx.children != []) {
                node.appendTree(...tx.children)
            }
        }
    }

    makeSelection(state) {
        this.selected = state
        for (let node of this.children) {
            node.makeSelection(state)
        }        
    }

    setEnabled(state) {
        this.enabled = state
        for (let node of this.children) {
            node.setEnabled(state)
        }
    }

    enableSelected(state) {
        if (this.selected) {
            this.setEnabled(state)
        } else {
            for (let node of this.children) {
                node.enableSelected(state)
            }
        } 
    }

    setMarked(state) {
        this.marked = state
        for (let child of this.children) {
            child.setMarked(state)
        }
    }

    cleanupMarked() {
        let enabled_children = []
        for (let child of this.children) {
            if (!child.marked) {
                child.cleanupMarked()
                enabled_children.push(child)
            }
        }
        this.children = enabled_children
    }
}


//......................................................................................

function count_tabs(word) {
    let count = 0
    for (let s of word) {
        if (s == '\t') count++
    }
    return count
}


//......................................................................................

function setCssVariables(node, variables) {
    for (const name in variables) {
        node.style.setProperty(`--${name}`, variables[name]);
    }
}

export function cssVariables(node, variables) {
    setCssVariables(node, variables);  
    return {
        update(variables) {
            setCssVariables(node, variables);
        }
    }
}

//......................................................................................