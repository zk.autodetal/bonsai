import * as XLSX from 'xlsx'


function read_excel_rows(buffer) {
    let workbook = XLSX.read(buffer)
    let worksheet = workbook.Sheets[workbook.SheetNames[0]]
    return XLSX.utils.sheet_to_json(worksheet)
}

onmessage = async function(evt) {
    let excel_data = read_excel_rows(await evt.data.arrayBuffer())
    postMessage(excel_data)
}